from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class WiFiData(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    ssid = db.Column(db.String(80), nullable=False)
    bssid = db.Column(db.String(80), nullable=False)
    rssi = db.Column(db.Integer, nullable=False)
    room = db.Column(db.String(80), nullable=False)


# Load the data from the database
from sqlalchemy import create_engine
import pandas as pd
import os


# Define the path to the database
db_path = os.path.join("instance", "wifi_data.db")
# Create an engine to connect to the database
engine = create_engine(f"sqlite:///{db_path}")
# Load data into a DataFrame
df = pd.read_sql_table("wi_fi_data", con=engine)
# print(df.head())
# print(df.shape)
# print(df['room'].value_counts())

# ------------------------------------------------

from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split

# Encode categorical variables
le_ssid = LabelEncoder()
le_bssid = LabelEncoder()
le_room = LabelEncoder()

df["ssid"] = le_ssid.fit_transform(df["ssid"])
df["bssid"] = le_bssid.fit_transform(df["bssid"])
df["room"] = le_room.fit_transform(df["room"])

# Split data into features and target
X = df[["ssid", "bssid", "rssi"]]
y = df["room"]

# Split into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.2, random_state=42
)

# ------------------------------------------------

from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score

# Train a Random Forest model
rf_model = RandomForestClassifier(n_estimators=100, random_state=42)
rf_model.fit(X_train, y_train)

# Predict on the test set
y_pred = rf_model.predict(X_test)

# Evaluate the model
accuracy = accuracy_score(y_test, y_pred)
print(f"Accuracy: {accuracy:.2f}")


# ------------------------------------------------

# # New sample
# new_sample = pd.DataFrame({
#     'ssid': le_ssid.transform(['Pod-O-IoT']),
#     'bssid': le_bssid.transform(['c8:84:8c:48:40:02']),
#     'rssi': [-72]
# })

# New sample (Kitchen)
new_sample = pd.DataFrame(
    {
        "ssid": le_ssid.transform(["LAPTOP-SMDK70OP"]),
        "bssid": le_bssid.transform(["68:ff:7b:18:b6:90"]),
        "rssi": [-72],
    }
)

# Convert new_sample DataFrame to numpy array
# new_sample_array = new_sample.to_numpy()

# Predict the room
predicted_room = rf_model.predict(new_sample)
predicted_room_name = le_room.inverse_transform(predicted_room)
print(f"Predicted Room: {predicted_room_name[0]}")
