import network
import time
import ujson
import urequests
import machine

# WiFi configuration
ssid = "egor2.4"       # Your WiFi SSID
password = ""  # Your WiFi password
server_url = "http://192.168.0.194:5000/api/wifi-data"
room = "Living Room"  # Set the room where scanning happens
led = machine.Pin("LED", machine.Pin.OUT)

# Connect to WiFi
def connect_wifi():
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    wlan.connect(ssid, password)
    while not wlan.isconnected():
        led.on()
        time.sleep(0.5)
        led.off()
        time.sleep(0.5)
    print('Network config:', wlan.ifconfig())
    led.off()

# Function to scan WiFi networks
def scan_wifi():
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    networks = wlan.scan()
    wifi_data = []
    for net in networks:
        wifi_data.append({
            "ssid": net[0].decode('utf-8'),
            "bssid": ':'.join(['%02x' % b for b in net[1]]),
            "rssi": net[3],
            "room": room
        })
    return wifi_data

# Function to send WiFi data to server
def send_wifi_data(data):
    try:
        headers = {'Content-Type': 'application/json'}
        response = urequests.post(server_url, data=ujson.dumps(data), headers=headers)
        print('Response:', response.text)
        response.close()
    except Exception as e:
        print('Failed to send data:', str(e))

# Main loop
connect_wifi()
while True:
    data = scan_wifi()
    send_wifi_data(data)
    time.sleep(5)  

