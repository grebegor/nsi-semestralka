# Indoor navigation using Rasspberry Pico

## Setup

### Activate virtual environment

```bash
source venv/bin/activate
```

### Install dependencies

```bash
pip install requirements.txt
```

### Start the app

```python
python3 app.py
```

### Start collecting data from Pico with MicroPython

### After collecting data, run the script to train the model

```python3
python3 predict.py
```
