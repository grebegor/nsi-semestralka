import sqlite3
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine
import pandas as pd
import os
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score

# Initialize the Flask-SQLAlchemy database
db = SQLAlchemy()


class WiFiData(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    ssid = db.Column(db.String(80), nullable=False)
    bssid = db.Column(db.String(80), nullable=False)
    rssi = db.Column(db.Integer, nullable=False)
    room = db.Column(db.String(80), nullable=False)


# Define the path to the database
db_path = os.path.join("instance", "wifi_data.db")
# Create an engine to connect to the database
engine = create_engine(f"sqlite:///{db_path}")
# Load data into a DataFrame
df = pd.read_sql_table("wi_fi_data", con=engine)

# Encode categorical variables
le_ssid = LabelEncoder()
le_bssid = LabelEncoder()
le_room = LabelEncoder()

df["ssid"] = le_ssid.fit_transform(df["ssid"])
df["bssid"] = le_bssid.fit_transform(df["bssid"])
df["room"] = le_room.fit_transform(df["room"])

# Split data into features and target
X = df[["ssid", "bssid", "rssi"]]
y = df["room"]

# Split into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.2, random_state=42
)

# Train a Random Forest model
rf_model = RandomForestClassifier(n_estimators=100, random_state=42)
rf_model.fit(X_train, y_train)

# Predict on the test set
y_pred = rf_model.predict(X_test)

# Evaluate the model
accuracy = accuracy_score(y_test, y_pred)
print(f"Random Forest Accuracy: {accuracy:.2f}")

# Extract samples for each room
room_samples = (
    df.groupby("room").apply(lambda x: x.sample(min(len(x), 5))).reset_index(drop=True)
)

# Predict the room for each sample
room_samples_features = room_samples[["ssid", "bssid", "rssi"]]
predicted_rooms = rf_model.predict(room_samples_features)
predicted_room_names = le_room.inverse_transform(predicted_rooms)

# Add predictions to the DataFrame
room_samples["predicted_room"] = predicted_room_names

# Print the predictions
for idx, row in room_samples.iterrows():
    actual_room = le_room.inverse_transform([row["room"]])[0]
    print(f"Actual Room: {actual_room}, Predicted Room: {row['predicted_room']}")

# Save the sample predictions to a CSV file for further inspection if needed
room_samples.to_csv("room_samples_predictions.csv", index=False)
