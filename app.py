from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
import json

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///wifi_data.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

class WiFiData(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    ssid = db.Column(db.String(80), nullable=False)
    bssid = db.Column(db.String(17), nullable=False)
    rssi = db.Column(db.Integer, nullable=False)
    room = db.Column(db.String(50), nullable=False)
    timestamp = db.Column(db.DateTime, default=datetime.utcnow)

appHasRunBefore = False

@app.before_request
def create_tables():
    global appHasRunBefore
    if not appHasRunBefore:
        db.create_all()
        appHasRunBefore = True


@app.route('/api/wifi-data', methods=['POST'])
def receive_wifi_data():
    if request.is_json:
        data = request.get_json()
        if isinstance(data, str):
            try:
                data = json.loads(data)
            except Exception as e:
                return jsonify({"message": f"Error parsing JSON: {e}"}), 400
        if not isinstance(data, list):
            return jsonify({"message": "JSON data should be a list of dictionaries"}), 400
        try:
            for item in data:
                if not all(k in item for k in ('ssid', 'bssid', 'rssi', 'room')):
                    return jsonify({"message": "Missing keys in JSON data"}), 400
                wifi_entry = WiFiData(
                    ssid=item['ssid'],
                    bssid=item['bssid'],
                    rssi=item['rssi'],
                    room=item['room']
                )
                db.session.add(wifi_entry)
            db.session.commit()
            return jsonify({"message": "Data received and inserted"}), 201
        except Exception as e:
            db.session.rollback()
            return jsonify({"message": f"An error occurred: {e}"}), 500
    else:
        return jsonify({"message": "Request body must be JSON"}), 400

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
